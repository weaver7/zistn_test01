sap.ui.define([
    "./BaseController"
],
/**
 * @param {typeof sap.ui.core.mvc.Controller} Controller
 */
function (Controller) {
    "use strict";
    return Controller.extend("com.istn.zistn_test01.controller.Main", {
        onInit: function () {                

        }
    });
});