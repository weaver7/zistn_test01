sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/core/UIComponent"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller, UIComponent) {
		"use strict";
		return Controller.extend("com.istn.zistn_test01.controller.BaseController", {

 		});
	});
